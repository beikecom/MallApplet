// components/sticky/sticky.js
Component({
  data: {},
  properties: {
    sticky: {
      type: Object,
      value: {
        "left": "",
        "right": ""
      }
    }
  },
  methods: {
    switchNear() {
      this.triggerEvent("switch");
    }

  }
});