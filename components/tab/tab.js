Component({
  properties: {
    tabItem: {
      type: Array,
      value: []
    }
  },
  data: {},
  methods: {
    goFilter(e) {
      const title = e.currentTarget.dataset.title;
      const category = e.currentTarget.dataset.category;
      console.log(category);
      tt.navigateTo({
        url: `/pages/card/filter/filter?title=${title}&category=${category}`,

        success(res) {// 跳转成功
        },

        fail(res) {
          tt.showToast({
            title: '跳转失败',
            success: res => {}
          });
          console.log(res);
        }

      });
    }

  },
  ready: function () {}
});