Component({
  properties: {
      tabList: {
          type: Array,
          value: [["招工", "", true], ["找活", "", false]], //["人脉集市", "", true], 
      },
      tabItem: {
          type: Array,
          value: []
      },
  },
  data: {

  },
  methods: {
      tabClick(e) {
          const current = e.currentTarget.dataset.index;
          let list = this.data.tabList;
          // 设置Tab数据
          list.forEach((item, index) => {
              if (index === current) item[2] = true;
              else item[2] = false;
          });
          // 设置Tabs数据 -- 父子组件通讯
          this.triggerEvent("setSwipterData", current);
          // 赋值数据
          this.setData({
              tabList: list
          });
      }
  },
  ready: function () {

  }
});