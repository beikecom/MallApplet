// components/cmap/cmap.js
var amapFile = require('../../utils/amap-wx.js');

Component({
  lifetimes: {
    attached: function () {
      var _this = this;
      var myAmapFun = new amapFile.AMapWX({
        key: '1fde56f1927c48a4ab60f5f4bd61f864'
      });
      myAmapFun.getPoiAround({
        success: function (data) {
          _this.setData({
            poisData: data.poisData
          });
        },
        fail: function (info) {
          //失败回调
          console.log(info);
        }
      });
    }
  },

  /**
   * 组件的属性列表
   */
  properties: {},

  /**
   * 组件的初始数据
   */
  data: {
    poisData: [],
    index: 0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    bindPickerChange: function (e) {
      let index = e.detail.value;
      this.setData({
        index
      });
      this.triggerEvent('getLocal', this.data.poisData[index]);
    }
  }
});
