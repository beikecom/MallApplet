// components/search/index.js
Component({
  data: {},
  properties: {
    placeholder: {
      type: String,
      value: "搜索"
    },
    searchStyle: {
      type: String,
      value: "radius"
    },
    textAlign: {
      type: String,
      value: "flex-start"
    },
    url: {
      type: String,
      value: ""
    },
    focus: {
      type: Boolean,
      value: !1
    },
    keyword: {
      type: String,
      value: ""
    }
  },
  data: {
    keyword: "",
    searchStyleObj: {
      square: "0rpx",
      radius: "12rpx",
      circle: "32rpx"
    }
  },
  methods: {
    setValue: function (t) {
      var e = t.detail.value;
      this.triggerEvent("setValue", e);
    },
    clear: function () {
      this.setData({
        keyword: ""
      });
    },
    confirm: function (t) {
      var e = this.data.keyword;
      this.triggerEvent("confirm", e);
    },
    back: function () {
      tt.navigateBack({
        delta: 1
      });
    },
    focus: function (e) {},
    goUrl: function () {
      var t = this.properties.url;
      tt.navigateTo({
        url: t
      });
    }
  }
});