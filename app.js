App({
  globalData: {
    userInfoParam: {},
    baseUrl: 'https://we9.91gongju.cn:10000',
    //baseUrl: 'https://localhost:10000',
    a: {}
  },

  onLaunch: function (z) {
    let a = z.query;
    this.globalData.a = a;
  },

  /**
   * 当小程序启动，或从后台进入前台显示，会触发 onShow
   */
  onShow: function (options) {},
 onShareAppMessage: function (shareOption) {
　　　　　　let that = this
　　　　　　if (shareOption.from == 'button') {  // 判断是按钮还是右上角分享
　　　　　　　　return {
　　　　　　　　　　channel: 'video',    　　　　　　　　// 必写 video
　　　　　　　　　　templateId: '',　　　　　　　　　　// 分享的模版 id （如果未设置就是默认，下面会说如何设置）
　　　　　　　　　　title: '',　　　　　　　　　　　　　// 分享的标题
　　　　　　　　　　desc: '',　　　　　　　　　　　　// 分享的内容介绍
　　　　　　　　　　path: `/pages/index/index`,　　// 分享的路径
　　　　　　　　　　extra: {
　　　　　　　　　　　　videoTopics: ["话题一", "话题二"] // 只有抖音才会有的属性
　　　　　　　　　　},
　　　　　　　}
　　　　　　} else { // 右上角分享
　　　　　　　　return {
　　　　　　　　　　templateId: '',    　　　　　　　　//分享的模版 id
　　　　　　　　　　title: '',   　　　　　　　　　　　//分享的标题
　　　　　　　　　　desc: '',　　　　　　　　　　　// 分享的内容
　　　　　　　　　　path: `/pages/index/index`,　// 分享的路径
　　　　　　　　　}
　　　　　　　　}
　　　　　　},
  globalHandleLogin: function (e) {
    var that = this;
    that.getSettingInfo();
    that.login();
    // if(!tt.getStorageSync('userInfo')){
    //   that.getUserInfoData();
    // }
  },
  getSettingInfo(){
    var that = this;
    tt.getSetting({
      success(res) {
        console.log('调用成功',res);
        if(res.authSetting['scope.userInfo']){
          tt.getUserInfo({
            success(res) {
              console.log('getUserInfo 调用成功',res.userInfo);
                var userInfo = res.userInfo;
                tt.setStorageSync('userInfo',userInfo);
                tt.setStorageSync('nick_name',userInfo.nickName);
                tt.setStorageSync('avatar_url',userInfo.avatarUrl);
                tt.setStorageSync('gender',userInfo.gender);
                that.getIsUser();
            },
            fail(res) {
              console.log(`getUserInfo 调用失败`);
            },
          });
        }else{
            tt.openSetting({
              success(res) {
              console.log("openSetting",res)
              tt.getUserInfo({
            success(res) {
              console.log('getUserInfo 调用成功',res.userInfo);
                var userInfo = res.userInfo;
                tt.setStorageSync('userInfo',userInfo);
                tt.setStorageSync('nick_name',userInfo.nickName);
                tt.setStorageSync('avatar_url',userInfo.avatarUrl);
                tt.setStorageSync('gender',userInfo.gender);
                that.getIsUser();
            },
            fail(res) {
              console.log(`getUserInfo 调用失败`);
            },
          });
            },
          });
          tt.showToast({
            title: '请打开用户信息按钮',
            icon: 'none',
            duration: 4000
          });
        }
      }
    });
  },
  login: function(){
    tt.login({
      success(res) {
        console.log('login调用成功',res.code);
        var code = res.code;
        //获取用户openid
        tt.request({
          url: `https://we9.91gongju.cn:10000/oauth/dy/getOpenid/${code}`, // 目标服务器url
          method: 'GET',
          dataType:'json',
          header: {
            'content-type': 'application/json'
          },
          success: (res) => {
            console.log('成功获取openid',JSON.parse(res.data.message).openid)
            tt.setStorageSync('openid',JSON.parse(res.data.message).openid);
          },
          fail(res) {
            console.log('获取openid失败');
          }
        })
      },
      fail(res) {
        console.log(`login调用失败`);
      },
    });
  },
  getIsUser(){
    var that = this;
    tt.request({
      url: `https://we9.91gongju.cn:10000/user/selectIsUser/${tt.getStorageSync('openid')}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('数据库中是否存在用户数据',res.data);
        if(res.data.message === '用户不存在'){
          //用户注册
          that.register();
        }else{
            this.globalData.userInfoParam = tt.getStorageSync('userInfo')
            tt.reLaunch({
              url: '/pages/ucenter/index/index' // 指定页面的url
            });
        }
      }
    });
  },
  register: function(){
    tt.request({
      url: `https://we9.91gongju.cn:10000/user/register`, // 目标服务器url
      method: 'POST',
      dataType:'json',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        openid: tt.getStorageSync('openid'),
        nickName: tt.getStorageSync('nick_name'),
        avatarUrl: tt.getStorageSync('avatar_url'),
        gender: tt.getStorageSync('gender'),
      },
      success: (res) => {
        var constant = res.data.message
        this.globalData.userInfoParam = tt.getStorageSync('userInfo')
        console.log('注册成功',constant)
        this.addIntegral();
        this.invatation();
        tt.reLaunch({
          url: '/pages/ucenter/index/index' // 指定页面的url
        });
      }
    });
  },

  addIntegral: function(){
    tt.request({
      url: `https://we9.91gongju.cn:10000/user/getInfo/${tt.getStorageSync('openid')}`,
      header: {'content-type':'application/json'},
      method: 'GET',
      dataType: 'json',
      success: (result) => {
        console.log("result",result)
        tt.request({
          url: `https://we9.91gongju.cn:10000/integral/addIntegral/${result.data.dataMap.data.id}`,
          method: 'POST',
          dataType: 'json',
          success: (result) => {
            console.log('添加积分调用成功',result);
          },
          fail: () => {
            console.log('添加积分调用失败');
          }
        });
      },
      fail: () => {}
    });     
  },
  invatation() {
    let a = this.globalData.a;
    if (a.refUser) {
      tt.request({
        url: `https://we9.91gongju.cn:10000/integral`,
        complete: (b) => { },
        data: {
          "category": "1",
          "origin": "邀请用户",
          "user": a.r,
          "value": 2
        },
        method: "POST",
        success: (b) => {
          tt.request({
            url: `https://we9.91gongju.cn:10000/message/save`,
            complete: (c) => { },
            data: {
              "message": "邀请用户",
              "user": a.r,
            },
            fail: (c) => { },
            method: "POST",
            success: (c) => {
              tt.hideLoading({
                complete: (d) => {
                  tt.showToast({
                    title: '被邀请成功',
                  })
                },
              })
            },
          })
        },
      })
    }
  }
});