Object.defineProperty(exports, "__esModule", {
  value: !0
});

var _slicedToArray = function (e, t) {
  if (Array.isArray(e)) return e;
  if (Symbol.iterator in Object(e)) return function (e, t) {
    var n = [],
        r = !0,
        o = !1,
        i = void 0;

    try {
      for (var a, u = e[Symbol.iterator](); !(r = (a = u.next()).done) && (n.push(a.value), !t || n.length !== t); r = !0);
    } catch (e) {
      o = !0, i = e;
    } finally {
      try {
        !r && u.return && u.return();
      } finally {
        if (o) throw i;
      }
    }

    return n;
  }(e, t);
  throw new TypeError("Invalid attempt to destructure non-iterable instance");
},
    _validate = require("./validate.js"),
    _validate2 = _interopRequireDefault(_validate),
    _ttPromiseMin = require("./ttPromise.min.js"),
    _ttPromiseMin2 = _interopRequireDefault(_ttPromiseMin);

function _interopRequireDefault(e) {
  return e && e.__esModule ? e : {
    default: e
  };
}

function _asyncToGenerator(e) {
  return function () {
    var u = e.apply(this, arguments);
    return new Promise(function (i, a) {
      return function t(e, n) {
        try {
          var r = u[e](n),
              o = r.value;
        } catch (e) {
          return void a(e);
        }

        if (!r.done) return Promise.resolve(o).then(function (e) {
          t("next", e);
        }, function (e) {
          t("throw", e);
        });
        i(o);
      }("next");
    });
  };
}

function _toConsumableArray(e) {
  if (Array.isArray(e)) {
    for (var t = 0, n = Array(e.length); t < e.length; t++) n[t] = e[t];

    return n;
  }

  return Array.from(e);
}

exports.default = {
  Validate: _validate2.default,
  regeneratorRuntime: _ttPromiseMin2.default,
  formatTime: function (e, t) {
    var n = t || "YY-M-D h:m:s",
        r = this.formatNumber,
        o = e || new Date();
    "Date" !== Object.prototype.toString.call(o).slice(8, -1) && (o = new Date(e));
    var i = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "日", "一", "二", "三", "四", "五", "六"];
    return n.replace(/YY|Y|M|D|h|m|s|week|星期/g, function (e) {
      switch (e) {
        case "YY":
          return o.getFullYear();

        case "Y":
          return (o.getFullYear() + "").slice(2);

        case "M":
          return r(o.getMonth() + 1);

        case "D":
          return r(o.getDate());

        case "h":
          return r(o.getHours());

        case "m":
          return r(o.getMinutes());

        case "s":
          return r(o.getSeconds());

        case "星期":
          return "星期" + i[o.getDay() + 7];

        case "week":
          return i[o.getDay()];
      }
    });
  },
  formatNumber: function (e) {
    return (e = e.toString())[1] ? e : "0" + e;
  },
  ctDate: function (e) {
    var t = 864e5;
    if (!e) return "";
    var n,
        r = (n = Date.now() - (e = "number" == typeof e ? e : +new Date(e))) / 2592e6,
        o = n / (7 * t),
        i = n / t,
        a = n / 36e5,
        u = n / 6e4;
    return 1 <= r ? parseInt(r) + "个月前" : 1 <= o ? parseInt(o) + "个星期前" : 1 <= i ? parseInt(i) + "天前" : 1 <= a ? parseInt(a) + "个小时前" : 1 <= u ? parseInt(u) + "分钟前" : "刚刚发表";
  },
  typeOf: function (e) {
    return Object.prototype.toString.call(e).slice(8, -1);
  },
  isEmpty: function (e) {
    var t = "" === e || null == e || "NaN" === e || !1 === e,
        n = void 0,
        r = void 0;
    return t || (n = "Object" === this.typeOf(e) && Object.keys(e).length < 1, r = "Array" === this.typeOf(e) && e.length < 1), t || n || r;
  },
  checkAuth: function (n) {
    var r = this;
    return new Promise(function (t, e) {
      tt.getSetting({
        success: function (e) {
          e.authSetting["scope." + n] ? t(!0) : t(!1);
        },
        fail: function () {
          r.networkError();
        }
      });
    });
  },
  // auth: function (n) {
  //   return new Promise(function (t, e) {
  //     tt.getSetting({
  //       success: function (e) {
  //         e.authSetting["scope." + n] ? t(!0) : tt.authorize({
  //           scope: "scope." + n,
  //           success: function () {
  //             t(!0);
  //           },
  //           fail: function () {
  //             t(!1);
  //           }
  //         });
  //       }
  //     });
  //   });
  // },
  getLocation: function () {
    return new Promise(function (t, n) {
      tt.getLocation({
        success: function (e) {
          t(e);
        },
        fail: function (e) {
          n(e);
        }
      });
    });
  },
  getWeather: function () {
    var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "GoI7BxLpfvBEyf1TcMXCloi99Vov7flZ";
    new (require("./bmap-tt.min.js").BMaptt)({
      ak: e
    }).weather({
      success: function (e) {
        console.log(e);
      }
    });
  },
  getBmapLocation: function () {
    var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "GoI7BxLpfvBEyf1TcMXCloi99Vov7flZ",
        n = new (require("./bmap-tt.min.js").BMaptt)({
      ak: e
    });
    return new Promise(function (a, t) {
      n.regeocoding({
        success: function (e) {
          var t = e.originalData.result,
              n = t.location,
              r = n.lat,
              o = n.lng,
              i = t.formatted_address;
          a({
            name: i,
            latitude: r,
            longitude: o,
            address: i
          });
        },
        fail: function (e) {
          console.log(e), e.errMsg.includes("auth") ? t(e) : e.errMsg.includes("domain") ? tt.showModal({
            title: "获取定位失败",
            content: "请在小程序公众平台添加百度域名api.map.baidu.com",
            showCancel: !1
          }) : e.errMsg.includes("Referer") ? tt.showModal({
            title: "获取定位失败",
            content: "登录百度开放平台给ak添加白名单",
            showCancel: !1
          }) : util.showModal(e.errMsg);
        }
      });
    });
  },
  authFail: function () {
    var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "需要的";
    tt.showModal({
      title: "未授权",
      content: "为保证功能正常使用,点击「右上角」-「关于**」-「右上角」-「设置」,打开" + e + "权限后重试",
      showCancel: !1
    });
  },
  networkError: function (e) {
    var t = {
      0: "网络异常",
      1: "请求超时",
      500: "服务器错误",
      404: "请求地址错误"
    }[e.status];

    if (e.message.includes("domain")) {
      var n = e.request.url;
      return t = "小程序公众平台未添加域名" + this.getHostname(n), void this.showModal(t);
    }

    this.showFail(t);
  },
  showModal: function () {
    var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "服务器在调试~";
    tt.showModal({
      title: "提示",
      content: e,
      showCancel: !1
    });
  },
  showLoading: function () {
    var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "加载中";
    tt.showLoading({
      title: e,
      mask: !0
    });
  },
  showSuccess: function () {
    var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "操作成功";
    tt.showToast({
      title: e
    });
  },
  showFail: function () {
    var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : "操作失败";
    tt.showToast({
      title: e,
      icon: "none"
    });
  },
  hideLoading: function () {
    tt.hideLoading();
  },
  hideAll: function () {
    tt.hideLoading(), tt.stopPullDownRefresh(), tt.hideNavigationBarLoading();
  },
  getData: function (e) {
    return e.currentTarget.dataset;
  },
  getValue: function (e) {
    return e.detail.value;
  },
  goUrl: function (e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : "navigateTo";
    if (e) if (-1 < e.indexOf("tel:")) tt.makePhoneCall({
      phoneNumber: e.split(":")[1]
    });else {
      if (-1 < e.indexOf("http")) return e = encodeURIComponent(e), void tt.navigateTo({
        url: "/pages/common/webview/webview?url=" + e
      });

      if (0 == e.indexOf("tt")) {
        var n,
            r = "",
            o = "release",
            i = e.split(":");
        return 1 == i.length ? n = i[0] : 2 == i.length ? (n = i[0], r = i[1]) : 3 == i.length && (n = i[0], r = i[1], o = i[2]), void tt.navigateToMiniProgram({
          appId: n,
          path: r,
          extraData: {
            lb: "longbing"
          },
          envVersion: o,
          success: function (e) {}
        });
      }

      tt[t]({
        url: e
      });
    }
  },
  setOptions: function (e) {
    return encodeURIComponent(JSON.stringify(e));
  },
  getOptions: function (e) {
    return JSON.parse(decodeURIComponent(e));
  },
  getPage: function () {
    var e = 0 < arguments.length && void 0 !== arguments[0] ? arguments[0] : 0,
        t = getCurrentPages();
    return t[t.length - 1 + e];
  },
  pay: function (n) {
    var r = this;
    return new Promise(function (t, e) {
      tt.requestPayment({
        timeStamp: n.timeStamp,
        nonceStr: n.nonceStr,
        package: n.package,
        signType: n.signType,
        paySign: n.paySign,
        success: function (e) {
          t(!0);
        },
        fail: function (e) {
          r.showFail("支付失败");
        },
        complete: function (e) {
          console.log(e);
        }
      });
    });
  },
  deepCopy: function (e) {
    if (e instanceof Array) {
      for (var t = [], n = 0; n < e.length; ++n) t[n] = this.deepCopy(e[n]);

      return t;
    }

    if (e instanceof Function) return t = new Function("return " + e.toString())();

    if (e instanceof Object) {
      t = {};

      for (var n in e) t[n] = this.deepCopy(e[n]);

      return t;
    }

    return e;
  },
  getIds: function (e) {
    var t = [];
    return (e = e || []).forEach(function (e) {
      t.push(e.id);
    }), t.join(",");
  },
  searchSubStr: function (e, t) {
    for (var n = [], r = e.indexOf(t); -1 < r;) n.push(r), r = e.indexOf(t, r + 1);

    return n;
  },
  partition: function (e, i) {
    e.reduce(function (e, t) {
      var n = _slicedToArray(e, 2),
          r = n[0],
          o = n[1];

      return i(t) ? [[].concat(_toConsumableArray(r), [t]), o] : [r, [].concat(_toConsumableArray(o), [t])];
    }, [[], []]);
  },
  getUrlParam: function (e, t) {
    var n = new RegExp("(^|&)" + t + "=([^&]*)(&|$)"),
        r = e.split("?")[1].match(n);
    return null != r ? unescape(r[2]) : null;
  },
  getSceneParam: function (e) {
    if (!e) return {};
    var t = e.split("&"),
        n = {},
        r = !0,
        o = !1,
        i = void 0;

    try {
      for (var a, u = t[Symbol.iterator](); !(r = (a = u.next()).done); r = !0) {
        var s = a.value.split("=");
        n[s[0]] = s[1];
      }
    } catch (e) {
      o = !0, i = e;
    } finally {
      try {
        !r && u.return && u.return();
      } finally {
        if (o) throw i;
      }
    }

    return n;
  },
  debounce: function (r, o, i) {
    var a = null;
    return function () {
      var e = this,
          t = arguments;

      if (a && clearTimeout(a), i) {
        var n = !a;
        a = setTimeout(function () {
          a = null;
        }, o), n && r.apply(e, t);
      } else a = setTimeout(function () {
        r.apply(e, t);
      }, o);
    };
  },
  throttle: function (r, o, i) {
    if (1 === i) var a = 0;else if (2 === i) var u;
    return function () {
      var e = this,
          t = arguments;

      if (1 === i) {
        var n = Date.now();
        o < n - a && (r.apply(e, t), a = n);
      } else 2 === i && (u || (u = setTimeout(function () {
        u = null, r.apply(e, t);
      }, o)));
    };
  },
  noCardTip: function () {
    var n = this;
    return _asyncToGenerator(_ttPromiseMin2.default.mark(function e() {
      var t;
      return _ttPromiseMin2.default.wrap(function (e) {
        for (;;) switch (e.prev = e.next) {
          case 0:
            return e.next = 2, tt.pro.showModal({
              title: "提示",
              content: "你还没有名片",
              confirmText: "去创建"
            });

          case 2:
            if (t = e.sent, t.confirm) {
              e.next = 6;
              break;
            }

            return e.abrupt("return");

          case 6:
            n.goUrl("/pages/card/add/add");

          case 7:
          case "end":
            return e.stop();
        }
      }, e, n);
    }))();
  },
  getHostname: function (e) {
    var t = /^http(s)?:\/\/(.*?)\//;
    return e.replace(t, "Host/"), e = t.exec(e)[2];
  },
  getPageConfig: function (e) {
    var t = 1 < arguments.length && void 0 !== arguments[1] ? arguments[1] : [],
        n = {};

    for (var r in t) {
      var o = t[r];
      n[o] = e[o];
    }

    return n;
  }
};