// pages/login/login.js
let app = getApp();
Page({
  data: {
    disabled: true,
    btnstate: "default",
    account: "",
    password: ""
  },
  accountInput: function (e) {
    var content = e.detail.value;
    tt.setStorageSync('content', content);

    if (content != "") {
      this.setData({
        disabled: false,
        btnstate: "primary",
        account: content
      });
    }
  },
  pwdBlur: function (e) {
    var password = e.detail.value;
    tt.setStorageSync('password', password);

    if (password != "") {
      this.setData({
        password: password
      });
    }
  },
  login: function () {
    tt.request({
      url: `${app.globalData.baseUrl}/user/login`,
      // 目标服务器url
      method: 'POST',
      dataType: 'json',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        username: tt.getStorageSync('content'),
        password: tt.getStorageSync('password')
      },
      success: res => {
        let code = res.data.code;
        console.log(code);

        if (code === 200) {
          tt.reLaunch({
            url: `../../card/square/square`
          });
        }
      }
    });
  }
});