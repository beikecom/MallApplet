// pages/company/company.js
let app = getApp();
Page({
  data: {
    disabled: true,
    btnstate: "default"
  },
  accountBlur: function (e) {
    var content = e.detail.value;
    tt.setStorageSync('content', content);

    if (content != "") {
      this.setData({
        disabled: false,
        btnstate: "primary",
        account: content
      });
    }
  },
  pwdBlur: function (e) {
    var password = e.detail.value;
    tt.setStorageSync('password', password);

    if (password != "") {
      this.setData({
        password: password
      });
    }
  },
  nicknameBlur: function (e) {
    var nickname = e.detail.value;
    tt.setStorageSync('nickname', nickname);

    if (nickname != "") {
      this.setData({
        nickname: nickname
      });
    }
  },
  register: function () {
    tt.request({
      url: `${app.globalData.baseUrl}/user/addUser`,
      // 目标服务器url
      method: 'POST',
      dataType: 'json',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        username: tt.getStorageSync('content'),
        password: tt.getStorageSync('password'),
        nickname: tt.getStorageSync('nickname')
      },
      success: res => {
        let code = res.data.code;
        console.log(code);

        if (code === 200) {
          tt.reLaunch({
            url: `../login/login`
          });
        }
      }
    });
  }
});