// pages/info/add.js
const app = getApp();
Page({
  data: {
    ttlogin: false,
    category: ["论坛热门", "论坛笑话"],
    form: {
      title: "",
      claim: "",
      typeOfWork: "0",
      number: 0,
      name: "",
      company: "",
      phone: ""
    },
    id: "1",
    pay_switch: !0,
    objectIndex: 0
  },
  onLoad: function (options) {},
  onShow: function () {},

  submit(e) {
    let _this = this;

    tt.request({
      url: `${app.globalData.baseUrl}/release-work/work/1`,
      method: "POST",
      data: _this.data.form,
      success: res => {
        tt.navigateBack({
          success(res1) {
            tt.showToast({
              title: '发布成功-正在审核，预计两个工作日通过',
              icon: 'none',
              duration: 4000
            });
          }

        });
      }
    });
  },

  phone(e) {
    let form = this.data.form;
    form.phone = e.detail.value;
    this.setData({
      form
    });
  },

  number(e) {
    let form = this.data.form;
    form.number = e.detail.value;
    this.setData({
      form
    });
  },

  name(e) {
    let form = this.data.form;
    form.name = e.detail.value;
    this.setData({
      form
    });
  },

  company(e) {
    let form = this.data.form;
    form.company = e.detail.value;
    this.setData({
      form
    });
  },

  title(e) {
    let form = this.data.form;
    form.title = e.detail.value;
    this.setData({
      form
    });
  },

  chooseImage(e) {
    tt.chooseImage({
      success: res => {
        console.log(res.tempFilePaths);
        let form = this.data.form;
        form.imgs.push(res.tempFilePaths);
        this.setData({
          form
        });
      }
    });
  },

  handerInputChange(e) {
    let form = this.data.form;
    form.claim = e.detail.value;
    this.setData({
      form
    });
  },

  bindObjectPickerChange(e) {
    let form = this.data.form;
    form.typeOfWork = e.detail.value;
    this.setData({
      objectIndex: e.detail.value,
      form
    });
  }

});