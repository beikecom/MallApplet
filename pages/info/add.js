// pages/info/add.js
const app = getApp();

Page({
  data: {
    ttlogin: false,
    category: ["每日找活", "每日招工"],
    form: {
      title: "",
      claim: "",
      typeOfWork: "0",
      number: 0,
      name: "",
      company: "",
      phone: "",
    },
    formPlaceHolder: {
      title: "工作意向",
      claim: "个人介绍",
      typeOfWork: "0",
      number: "年龄",
      name: "我的名字",
      company: "技能",
      phone: "我的手机号",
    },
    id: "1",
    pay_switch: !0,
    objectIndex: 0
  },
  onLoad: function (options) {
    var that = this;
    var userInfoParam = tt.getStorageSync('userInfo')
    console.log(userInfoParam);
    if (userInfoParam) {
      that.setData({
        ttlogin: true,
        apiUserInfoMap: userInfoParam
      });
    } else {
      that.setData({
        ttlogin: false
      });
    }
  },
  onShow: function () {

  },

  cancelLogin: function () {
    tt.navigateBack({
      delta: 1,
      success(res) {
        console.log(`${res}`);
      },
      fail(res) {
        console.log(`navigateBack调用失败`);
      },
    });
  },
  submit(e) {
    let _this = this;
    let openid = tt.getStorageSync('openid');
    console.log('openid', openid);
    tt.request({
      url: `${app.globalData.baseUrl}/user/selectIsUser/${openid}`, // 目标服务器url
      method: "get",
      success(res) {
        console.log("suc" + res);
        console.log(res);
        _this.setData({ id: res.data.dataMap.data.id });
        let id = _this.data.id;
        for (let key in _this.data.form) {
          if (_this.data.form[key] == "" || _this.data.form[key] == undefined || _this.data.form[key] == null) {
            tt.showToast({
              title: '有选项为空',
              icon: "none"
            });
            return false;
          }
        }
        tt.request({
          url: `${app.globalData.baseUrl}/release-work/work/${id}`,
          method: "POST",
          data: _this.data.form,
          success: (res) => {
            tt.navigateBack({
              success(res1) {
                tt.showToast({
                  title: '发布成功-正在审核，预计两个工作日通过',
                  icon: 'none',
                  duration: 4000
                });
              }
            });
          }
        });
      },
      fail(res) {
        tt.showToast({
          title: '服务器真的错误了！', // 内容
          icon: "none",
        });
      },
      complete(res) {
        if (res.data.code != 200) {
          tt.showToast({
            title: '服务器错误了！', // 内容
            icon: "none",
          });
        }
      }
    });
  },
  phone(e) {
    let form = this.data.form;
    form.phone = e.detail.value;
    this.setData({ form })
  },
  number(e) {
    let form = this.data.form;
    form.number = e.detail.value;
    this.setData({ form })
  },
  name(e) {
    let form = this.data.form;
    form.name = e.detail.value;
    this.setData({ form })
  },
  company(e) {
    let form = this.data.form;
    form.company = e.detail.value;
    this.setData({ form })
  },
  title(e) {
    let form = this.data.form;
    form.title = e.detail.value;
    this.setData({ form })
  },
  chooseImage(e) {
    tt.chooseImage({
      success: (res) => {
        console.log(res.tempFilePaths);
        let form = this.data.form;
        form.imgs.push(res.tempFilePaths);
        this.setData({ form })
      }
    });
  },
  handerInputChange(e) {
    let form = this.data.form;
    form.claim = e.detail.value;
    this.setData({ form })
  },
  bindObjectPickerChange(e) {
    if (e.detail.value == 0) {
      this.setData({
        formPlaceHolder: {
          title: "工作意向",
          claim: "个人介绍",
          typeOfWork: "0",
          number: "年龄",
          name: "我的名字",
          company: "技能",
          phone: "我的手机号",
        }
      });
    } else {
      this.setData({
        formPlaceHolder: {
          title: "名称",
          claim: "招工要求",
          typeOfWork: "0",
          number: "招工人数",
          name: "联系人",
          company: "公司",
          phone: "联系人手机号",
        }
      });
    }
    let form = this.data.form;
    form.typeOfWork = e.detail.value;
    this.setData({ objectIndex: e.detail.value, form })
  },


  //授权登录绑定按钮
  processLogin: function (e) {
    app.globalHandleLogin(e);
    this.setData({
      apiUserInfoMap: app.globalData.userInfoData
    });
    console.log('qwe', app.globalData.userInfoData);
  }
  // processLogin: function (e) {
  //   var that = this;
  //   console.log('111', e.detail.userInfo);
  //   //登录
  //   that.login();
  //   if (!wx.getStorageSync('userInfo')) {
  //     //获取用户信息
  //     console.log('用户信息', e.detail.userInfo);
  //     that.setData({
  //       apiUserInfoMap: e.detail.userInfo
  //     });
  //     wx.setStorageSync('userInfo', e.detail.userInfo)
  //     wx.setStorageSync('openid', e.detail.userInfo.openid);
  //     wx.setStorageSync('nick_name', e.detail.userInfo.nickName);
  //     wx.setStorageSync('avatar_url', e.detail.userInfo.avatarUrl);
  //     wx.setStorageSync('gender', e.detail.userInfo.gender);
  //   } else if (wx.getStorageSync('userInfo')) {
  //     that.setData({
  //       apiUserInfoMap: wx.getStorageSync('userInfo'),
  //       ttlogin: true
  //     });
  //   }
  // },

  // //登录
  // login: function () {
  //   var that = this;
  //   wx.login({
  //     success(res) {
  //       console.log('login调用成功', res.code);
  //       var code = res.code;
  //       //获取用户openid
  //       wx.request({
  //         url: `${app.globalData.baseUrl}/oauth/wx/getOpenid/${code}`,
  //         method: 'GET',
  //         dataType: 'json',
  //         header: {
  //           'content-type': 'application/json'
  //         },
  //         success: (res) => {
  //           console.log('成功获取openid', JSON.parse(res.data.message).openid)
  //           wx.setStorageSync('openid', JSON.parse(res.data.message).openid);
  //           console.log('查询数据库中是否有该用户数据', wx.getStorageSync('openid'));
  //           wx.request({
  //             url: `${app.globalData.baseUrl}/user/selectIsUser/${wx.getStorageSync('openid')}`, // 目标服务器url
  //             method: 'GET',
  //             dataType: 'json',
  //             header: {
  //               'content-type': 'application/json'
  //             },
  //             success: (res) => {
  //               console.log('数据库中是否存在用户数据', res.data);
  //               if (res.data.message === '用户不存在') {
  //                 //用户注册
  //                 that.register();
  //               } else {
  //                 that.setData({
  //                   apiUserInfoMap: wx.getStorageSync('userInfo'),
  //                   ttlogin: true
  //                 });
  //               }
  //             }
  //           });
  //         },
  //         fail(res) {
  //           console.log('获取openid失败');
  //         }
  //       })
  //     },
  //     fail(res) {
  //       console.log(`login调用失败`);
  //     },
  //   });
  // },

  // //用户注册
  // register: function () {
  //   wx.request({
  //     url: `${app.globalData.baseUrl}/user/register`, // 目标服务器url
  //     method: 'POST',
  //     dataType: 'json',
  //     header: {
  //       'content-type': 'application/x-www-form-urlencoded'
  //     },
  //     data: {
  //       openid: wx.getStorageSync('openid'),
  //       nickName: wx.getStorageSync('nick_name'),
  //       avatarUrl: wx.getStorageSync('avatar_url'),
  //       gender: wx.getStorageSync('gender'),
  //     },
  //     success: (res) => {
  //       var constant = res.data.message
  //       console.log('注册成功', constant)
  //     }
  //   });
  // }
})