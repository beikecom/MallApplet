// pages/common/category.js
let app = getApp();
Page({
  data: {
    content: {}
  },
  onLoad: function (options) {
    let id = options.id;
    tt.request({
      url: `${app.globalData.baseUrl}/release-work/work/one/${id}`,
      // 目标服务器url
      success: res => {
        let content = res.data.dataMap.data;
        this.setData({
          content
        });
      }
    });
  }
});