// pages/common/category.js
const app = getApp();
Page({
  data: {
    ttlogin: true,
    content: {},
    userInfo: {},
    collect: {},
    status: false,
    integral: 0,
    a: {
      "message": "查看手机号-",
      "user": "1",
      "work": ""
    },
    b: {
      "category": "0",
      "origin": "查看手机号",
      "user": "1",
      "value": 1
    },
    statusPhone: false,
    formPlaceHolder: {
      title: "名称",
      claim: "招工要求",
      typeOfWork: "0",
      number: "招工人数",
      name: "联系人",
      company: "公司",
      phone: "手机号",
    }
  },
  onPullDownRefresh: function () {
    let id = this.data.content.id;
    tt.request({
      url: `${app.globalData.baseUrl}/release-work/work/one/${id}`, // 目标服务器url
      success: (res) => {
        let content = res.data.dataMap.data;
        if (content) {
          this.onGetUserInfo(id);
          this.setData({ content });
        } else {
          tt.showModal({
            complete: (res) => { },
            content: '没有查到数据，可能已经删除',
            fail: (res) => { },
            showCancel: false,
            success: (result) => {
              tt.navigateBack();
            },
            title: '数据不存在',
          });
        }
      },
      complete: (res) => { tt.stopPullDownRefresh() },
    });
  },
  getPhone() {
    let a = this.data.a;
    let b = this.data.b;
    let user = this.data.userInfo.id;
    let work = this.data.content.id;
    //
    var userInfoParam = tt.getStorageSync('userInfo')
    console.log('userInfoParam',userInfoParam);
    if(userInfoParam){
      this.setData({
        ttlogin: true,
        apiUserInfoMap: userInfoParam
      });
    }else{
      this.setData({
        ttlogin: false
      });
      tt.showToast({
        title: '授权后请下拉刷新当前页面',
        icon: "none"
      })
      return 0;
    }
    //
    if (this.data.integral == 0) {
      tt.showModal({
        cancelText: '返回列表',
        complete: (res) => { },
        confirmText: '获取积分',
        content: '您当前的积分不足，只需一个积分即可查看完整电话，是否前往获取积分？',
        fail: (res) => { },
        showCancel: true,
        success: (result) => {
          if (result.cancel) {
            tt.navigateBack();
          }
          if (result.confirm) {
            tt.navigateTo({
              url: '/pages/common/share/share',
            });
          }
        },
        title: '温馨提示',
      })
    } else {
      a.user = user;
      a.message = "查看手机号-" + this.data.content.title
      a.work = work;
      b.user = user;
      tt.showLoading({
        title: '获取手机号',
      })
      tt.request({
        url: `${app.globalData.baseUrl}/bill`,
        complete: (res) => {
          tt.hideLoading();
          tt.startPullDownRefresh();
        },
        data: { 'user': user, 'work': work },
        fail: (res) => { },
        method: "POST",
        success: (result) => {
          tt.request({
            url: `${app.globalData.baseUrl}/integral`,
            complete: (res) => {
            },
            data: b,
            fail: (res) => {
              tt.showToast({
                title: '网络错误',
                icon: "none",
                duration: 4000
              });
            },
            method: "POST",
            success: (result) => {
              tt.request({
                url: `${app.globalData.baseUrl}/message/save`,
                data: a,
                method: "POST",
              });
            },
          });
        },
      })
    }
  },
  
  collect(e) {
    let user = this.data.userInfo.id;
    let work = this.data.content.id;
    let typeOfWork = this.data.content.typeOfWork;
    if (user == null) {
      tt.showToast({
        title: '登录才可以收藏',
        icon: "none"
      })
    } else {
      if (this.data.status) {
        this.remove();
        this.setData({ status: false })
      } else {
        this.add(user, work, typeOfWork);
        this.setData({ status: true })
      }
    }
  },
  onLoad: function (options) {
    let id = options.id;
    tt.request({
      url: `${app.globalData.baseUrl}/release-work/work/one/${id}`, // 目标服务器url
      success: (res) => {
        let content = res.data.dataMap.data;
        if (content) {
          this.onGetUserInfo(id);
          this.setData({ content });
          if (content.typeOfWork == 0) {
            this.setData({
              formPlaceHolder: {
                title: "工作意向",
                claim: "个人介绍",
                typeOfWork: "0",
                number: "年龄",
                name: "姓名",
                company: "技能",
                phone: "手机号",
              }
            });
            tt.setNavigationBarTitle({
              title: '名片详情'
            });
          }
        } else {
          tt.showModal({
            complete: (res) => { },
            content: '没有查到数据，可能已经删除',
            fail: (res) => { },
            showCancel: false,
            success: (result) => {
              tt.navigateBack();
            },
            title: '数据不存在',
          });
        }
      }
    });
    tt.showShareMenu({
      withShareTicket: true
    });
  },
  onShow: function () {
    tt.showShareMenu({
      withShareTicket: true
    });
  },
  getIntegral(user) {
    tt.request({
      url: `${app.globalData.baseUrl}/integral/sum/${user}`,
      complete: (res) => { },
      fail: (res) => { },
      success: (result) => {
        this.setData({ integral: result.data.dataMap.data });
      },
    })
  },
  getPhoneStatus(user, work) {
    let _this = this;
    tt.request({
      url: `${app.globalData.baseUrl}/user-release-work/one/${work}`,
      success(res) {
        if (res.data.dataMap.data!=null) {
          if (res.data.dataMap.data.userId != user) {
            tt.request({
              url: `${app.globalData.baseUrl}/bill/one/${user}/${work}`,
              complete: (res) => { },
              fail: (res) => { },
              success: (result) => {
                let bill = result.data.dataMap.data;
                if (bill != null) {
                  _this.setData({ statusPhone: true });
                }
              },
            })
          } else {
            _this.setData({ statusPhone: true });
          }
        } else {
          tt.request({
            url: `${app.globalData.baseUrl}/bill/one/${user}/${work}`,
            complete: (res) => { },
            fail: (res) => { },
            success: (result) => {
              let bill = result.data.dataMap.data;
              if (bill != null) {
                _this.setData({ statusPhone: true });
              }
            },
          })
        }
      }
    })
  },
  getSatus: function (user, work) {
    let _this = this;
    tt.showLoading({
      title: '加载中',
    })
    tt.request({
      url: `${app.globalData.baseUrl}/collect/${user}/${work}`,
      complete: (res) => { tt.hideLoading() },
      fail: (res) => { },
      success: (result) => {
        if (result.data.dataMap.data == null || result.data.dataMap.data == undefined) {
          _this.setData({ status: false })
        } else {
          _this.setData({ status: true, collect: result.data.dataMap.data })
        }
      },
    });
  },
  onGetUserInfo(id) {
    let openid = tt.getStorageSync('openid');
    let _this = this;
    if (!openid) {
      tt.showToast({
        title: '未登录',
        icon: "none"
      })
    } else {
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openid}`,
        complete: (res) => { },
        success: (result) => {
          _this.setData({ userInfo: result.data.dataMap.data });
          let userInfo = _this.data.userInfo;
          _this.getSatus(userInfo.id, id);
          _this.getIntegral(userInfo.id);
          _this.getPhoneStatus(userInfo.id, _this.data.content.id)
        },
      })
    }
  },
  add(user, work, typeOfWork) {
    tt.showLoading({
      title: '收藏中',
    });
    let from = { typeOfWork, user, work }
    tt.request({
      url: `${app.globalData.baseUrl}/collect/save`,
      complete: (res) => { tt.hideLoading() },
      data: from,
      fail: (res) => { },
      method: "POST",
      success: (result) => { },
    })
  },
  remove() {
    let _this = this;
    tt.showLoading({
      title: '取消收藏',
    });
    tt.request({
      url: `${app.globalData.baseUrl}/collect?id=${_this.data.collect.id}`,
      complete: (res) => { tt.hideLoading() },
      fail: (res) => { },
      method: "DELETE",
      success: (result) => { },
    })
  },
  onShareAppMessage: function (res) {
    let _this = this;
    if (res.from === 'button') {
      // 来自页面内转发按钮
    }
    return {
      title: `${_this.data.content.title}`,
      path: `/pages/common/detail/detail?id=${_this.data.content.id}`,
    }
  },
  onShareTimeline: function (res) {

  },
  //授权登录绑定按钮
  processLogin: function(e){
    app.globalHandleLogin(e);
    this.setData({
      apiUserInfoMap: app.globalData.userInfoData
    });
    this.onLoad();
  }
})