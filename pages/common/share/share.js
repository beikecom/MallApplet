// pages/common/share/share.js
let app =  getApp();
  
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.onGetUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: '全国建筑工地招工服务平台',
      path: `/pages/card/square/square?r=${this.data.userInfo.id}`
    }
  },
  onGetUserInfo() {
    let openid = tt.getStorageSync('openid');
    let _this = this;
    if (!openid) {
      tt.navigateBack({
        complete: (res) => {
          tt.showToast({
            title: '请登录',
            icon: "none",
          })
        }
      });
    } else {
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openid}`,
        complete: (res) => { },
        success: (result) => {
          _this.setData({ userInfo: result.data.dataMap.data });
        },
      })
    }
  }
})