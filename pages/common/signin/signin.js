// pages/common/signin/signin.js
let app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    form: {
      isSignIn: false,
      title: "签到"
    },
    a: {
      "message": "签到成功",
      "user": "1",
    },
    b: {
      "category": "1",
      "origin": "签到",
      "user": "1",
      "value": 2
    },
    userInfo: {},
    integral: 0,
    status: false,
  },

  signin() {
    let form = this.data.form;
    let user = this.data.userInfo.id;
    let a = this.data.a;
    let b = this.data.b;
    if (form.isSignIn) {
      tt.showToast({
        title: '不可重复签到',
        icon: "none"
      })
    } else {
      a.user = user;
      b.user = user;
      tt.showLoading({
        title: '签到中',
      })
      tt.request({
        url: `${app.globalData.baseUrl}/integral`,
        complete: (res) => {
          tt.hideLoading();
        },
        data: b,
        fail: (res) => {
          tt.hideLoading();
          tt.showToast({
            title: '签到失败',
            icon: "none",
            duration: 4000
          });
        },
        method: "POST",
        success: (result) => {
          tt.request({
            url: `${app.globalData.baseUrl}/message/save`,
            data: a,
            method: "POST",
          });
          form.isSignIn = true;
          form.title = "已签到";
          this.setData({ form });
          tt.startPullDownRefresh();
        },
      });
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.onGetUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    this.onGetUserInfo();
    tt.stopPullDownRefresh()
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getStatus(user) {
    let _this = this;
    tt.request({
      url: `${app.globalData.baseUrl}/integral/states/${user}`,
      success: (result) => {
        let form = _this.data.form;
        if (result.data.dataMap.data.id) {
          form.isSignIn = true;
          form.title = "已签到";
          _this.setData({ form });
        } else {
          form.isSignIn = false;
          form.title = "签到";
          _this.setData({ form });
        }
      },
    })
  },
  getIntegral(user) {
    tt.request({
      url: `${app.globalData.baseUrl}/integral/sum/${user}`,
      complete: (res) => { },
      fail: (res) => { },
      success: (result) => {
        this.setData({ integral: result.data.dataMap.data });
      },
    })
  },
  onGetUserInfo() {
    let openid = tt.getStorageSync('openid');
    let _this = this;
    if (!openid) {
      tt.navigateBack({
        complete: (res) => {
          tt.showToast({
            title: '请登录',
            icon: "none",
          })
        }
      });
    } else {
      tt.showLoading({
        title: '加载中',
      })
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openid}`,
        complete: (res) => { tt.hideLoading() },
        success: (result) => {
          _this.setData({ userInfo: result.data.dataMap.data });
          _this.getIntegral(result.data.dataMap.data.id)
          _this.getStatus(result.data.dataMap.data.id);
        },
      })
    }
  }
})