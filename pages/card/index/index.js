let app =  getApp();
  
Page({
  data: {
    uinfo:{},
    personnelUserInfo: {},
    isPersonnelUserInfo: true,
    isPersonnel: true,
    personnel:{},
    project:[],
    skill:[]
  },
  //options(Object)
  onLoad: function(options) {
    tt.request({
      url: `${app.globalData.baseUrl}/user/getAllUserInfo/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('查询成功',res.data);
          this.setData({
            uinfo: res.data.dataMap.data,
          });
      }
    });

    wx.request({
      url: `${app.globalData.baseUrl}/user/getUserInfo/${wx.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('查询成功',res.data);
          this.setData({
            personnelUserInfo: res.data.dataMap.data,
          });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/user/getUserInfo/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('个人信息',res.data);
          this.setData({
            personnelUserInfo: res.data.dataMap.data,
            isPersonnelUserInfo: false
          });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/personnel-info/getInfo/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('人员信息',res.data);
        this.setData({
          personnel: res.data.dataMap.data,
          isPersonnel: false
        });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/project-experience/getProject/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('项目经验',res.data);
        this.setData({
          project: res.data.dataMap.data,
        });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/skills-certificate/getSkills/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('职业技能',res.data);
        this.setData({
          skills: res.data.dataMap.data,
        });
      }
    });
  },
  onShow: function() {
    tt.request({
      url: `${app.globalData.baseUrl}/user/getAllUserInfo/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('查询成功',res.data);
          this.setData({
            uinfo: res.data.dataMap.data,
          });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/user/getUserInfo/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('个人信息',res.data);
          this.setData({
            personnelUserInfo: res.data.dataMap.data,
          });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/personnel-info/getInfo/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('人员信息',res.data);
        this.setData({
          personnel: res.data.dataMap.data
        });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/project-experience/getProject/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('项目经验',res.data);
        this.setData({
          project: res.data.dataMap.data,
        });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/skills-certificate/getSkills/${tt.getStorageSync("openid")}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('职业技能',res.data);
        this.setData({
          skills: res.data.dataMap.data,
        });
      }
    });
  },
  onShareAppMessage: function(res) {
    return {
      title: '我的名片',
      desc: '分享',
    }
  },
  goUrl: function(){
    tt.navigateTo({
      url: '/pages/ucenter/recruit/recruit'
    });
  },
  handleCall: function(e){
    tt.makePhoneCall({
      phoneNumber: this.data.uinfo.phone,
    });
  }
});
  