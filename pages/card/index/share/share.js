let app =  getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    uinfo:{},
    personnel: true,
    isPersonnel: true,
    project:[],
    skill:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    console.log(options)
    tt.request({
      url: `${app.globalData.baseUrl}/user/getAllUserInfo/${options.openId}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('查询成功',res.data);
          this.setData({
            uinfo: res.data.dataMap.data,
          });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/user/getUserInfo/${options.openId}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('个人信息',res.data);
          this.setData({
            personnelUserInfo: res.data.dataMap.data,
            isPersonnelUserInfo: false
          });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/personnel-info/getInfo/${options.openId}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('人员信息',res.data);
        this.setData({
          personnel: res.data.dataMap.data,
          isPersonnel: false
        });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/project-experience/getProject/${options.openId}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('项目经验',res.data);
        this.setData({
          project: res.data.dataMap.data,
        });
      }
    });

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/skills-certificate/getSkills/${options.openId}`, // 目标服务器url
      method: 'GET',
      dataType:'json',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('职业技能',res.data);
        this.setData({
          skills: res.data.dataMap.data,
        });
      }
    });
  },
  onShow: function() {

  },
  
  onShareAppMessage: function(res) {
    console.log('res',res);
    if (res.from === 'button') {
      // 来自页面内转发按钮
    }
    return {
      title: '我的名片',
      path: `/pages/card/index/share/share?openId=${res.target.dataset.id}`,
      desc: '分享',
    }
  },
  handleCall: function(e){
    console.log(e)
    tt.makePhoneCall({
      phoneNumber: e.currentTarget.dataset.id,
    });
  },
  blackHome: function(){
    tt.switchTab({
      url: '/pages/card/square/square' // 指定页面的url
    });
  }
})