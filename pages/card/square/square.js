const app = getApp();

Page({
  data: {
    tabItems: [],
    ftabItems: [["论坛热门", "", true], ["论坛笑话", "", false]],
    contents: [],
    fcontents: [],
    sticky: { "left": "热门名片", "right": "最新发布的名片" },
    fsticky: { "left": "热门帖子", "right": "最新发布" },
    addShow: true,
    isShow: false,
    typeOfWork: 1,
  },
  onLoad(option) {
    let _this = this;
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/is-show/select/isShow`,
      success: (res) => {
        const isShow = res.data.dataMap.data.isShow;
        if (isShow === 1) {
          _this.setData({ isShow: true });
        } else {
          _this.setData({ isShow: false });
        }
        if (this.data.isShow) {
          // 第二个数据
          let tab = [];
          tt.showLoading({
            title: '加载中'
          });
          let _this = this;
          tt.request({
            url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${this.data.typeOfWork}`,
            success: (res) => {
              let contents = _this.data.contents;
              contents = res.data.dataMap.data;
              _this.setData({ contents, sticky: { "left": "热门工作", "right": "最新发布的工作" }, });
            },
            fail(res) {
              tt.hideLoading({
                complete: (res) => {
                  tt.showToast({
                    title: '网络错误',
                    icon: "none"
                  })
                },
              });
            }
          });
          this.setData({
            tabItems: tab,
            addShow: true,
          });
          tt.hideLoading();
        } else {
          tt.setNavigationBarTitle({
            title: '工友论坛' // 导航栏标题
          });
          tt.request({
            url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${this.data.typeOfWork}`,
            success: (res) => {
              console.log(res);
              let fcontents = _this.data.contents;
              fcontents = res.data.dataMap.data.records;
              _this.setData({ fcontents });
            }
          });
        }
      }
    });
  },
  onShow: function () {
    let _this = this;
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/is-show/select/isShow`,
      success: (res) => {
        const isShow = res.data.dataMap.data.isShow;
        if (isShow === 1) {
          _this.setData({ isShow: true });
        } else {
          _this.setData({ isShow: false });
        }
        if (this.data.isShow) {
          // 第二个数据
          let tab = [];
          let _this = this;
          tt.request({
            url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${this.data.typeOfWork}`,
            success: (res) => {
              let contents = _this.data.contents;
              contents = res.data.dataMap.data;
              _this.setData({ sticky: { "left": "热门工作", "right": "最新发布的工作" }, });
            }
          });
          this.setData({
            tabItems: tab,
            addShow: true,
          });
        } else {
          tt.setNavigationBarTitle({
            title: '工友论坛' // 导航栏标题
          });
          tt.request({
            url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${this.data.typeOfWork}`,
            success: (res) => {
              let contents = _this.data.contents;
              contents = res.data.dataMap.data.records;
              _this.setData({ contents });
            }
          });
        }
      }
    });
  },
  
  // 下拉刷新
  onPullDownRefresh() {
    // 第二个数据
    let tab = [];
    tt.showLoading({
      title: '加载中'
    });
    //
    let _this = this;
    tt.request({
      url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${_this.data.typeOfWork}`,
      success: (res) => {
        let contents = _this.data.contents;
        contents = res.data.dataMap.data;
        _this.setData({ contents, sticky: { "left": "热门工作", "right": "最新发布的工作" }, });
      },
      complete: (res) => {
        tt.hideLoading(); tt.stopPullDownRefresh();
        _this.setData({
          tabItems: tab,
          addShow: true,
        });
      },
    });
  },
  setSwipterData(e) {
    const current = e.detail;
    if (this.data.isShow) {
      if (current === 100) {
        // 第一个数据
        tt.showLoading({
          title: '加载中', // 内容
        });
        let tab = [
          [
            ["通用工种", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3cdf2b997.png", ""],
            ["机械操作工", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-05/5d479c6fe8346.png", ""],
            ["施工管理", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3cf06cc1f.jpg", ""],
            ["路桥隧道", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3cfad4ba2.jpg", ""],
            ["桩基础", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3d07c9e71.jpg", ""],
            ["防护工程", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3d130f528.jpg", ""],
            ["市政园林", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3db618f73.jpg", ""],
            ["设计师", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3e5c19b50.jpg", ""],
            ["家具建材", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3e651be4c.jpg", ""],
            ["装配式建筑", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3e71d5af0.jpg", ""],
          ],
          [
            ["综合承包", "https://we7.91gongju.cn/attachment/upload/5/picture/2019-08-07/5d4a3d130f528.jpg", ""],
          ]
        ];
        this.setData({
          tabItems: [],  // 原来 tab
          addShow: false,
          contents: [],
          sticky: { "left": "热门名片", "right": "最新发布的名片" },
        });
        let _this = this;
        tt.request({
          url: `${app.globalData.baseUrl}/user/userInfo/${1}`,
          success: (res) => {
            if (res.data.code != 200) {
              // tt.showToast({
              //   title: res.data.message,
              //   icon: "none",
              //   success() {
              //     //console.log(res.data.dataMap.data)
              //   }
              // });
            } else {
              let contents = res.data.dataMap.data;
              _this.setData({ contents });
            }
          },
          fail(res) {
            tt.showToast({
              title: '加载失败', // 内容
              icon: "fail"
            });
          },
          complete() {
            tt.stopPullDownRefresh();
          }
        });
        tt.hideLoading();
      } else if (current === 0) {
        this.setData({ typeOfWork: 1 })
        // 第二个数据
        let tab = [];
        tt.showLoading({
          title: '加载中'
        });
        //
        let _this = this;
        tt.request({
          url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${this.data.typeOfWork}`,
          success: (res) => {
            let contents = _this.data.contents;
            contents = res.data.dataMap.data;
            _this.setData({ contents, sticky: { "left": "热门工作", "right": "最新发布的工作" }, });
          }
        });
        this.setData({
          tabItems: tab,
          addShow: true,
        });
        tt.hideLoading();
      } else if (current === 1) {
        this.setData({ typeOfWork: 0 })
        // 第三个
        tt.showLoading({
          title: '加载中',
          success: (res) => {

          }
        });
        let tab = [];
        let _this = this;
        tt.request({
          url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${this.data.typeOfWork}`,
          success: (res) => {
            let contents = _this.data.contents;
            contents = res.data.dataMap.data;
            _this.setData({ contents, sticky: { "left": "热门工作", "right": "最新发布的工作" }, });
          }
        });
        this.setData({
          tabItems: tab,
          addShow: true,
        });
        tt.hideLoading();
      }
    } else {
      let _this = this;
      switch (current) {
        case 0:
          tt.request({
            url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${1}`,
            success: (res) => {
              let fcontents = _this.data.contents;
              fcontents = res.data.dataMap.data.records;
              _this.setData({ fcontents });
            }
          });
          break;
        case 1:
          tt.request({
            url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${0}`,
            success: (res) => {
              let fcontents = _this.data.contents;
              fcontents = res.data.dataMap.data.records;
              _this.setData({ fcontents });
            }
          });
          break;
      }
    }
  },
  goDetail: function (e) {
    let id = e.currentTarget.dataset.id;
    tt.navigateTo({
      url: `/pages/common/detail/detail?id=${id}`
    });
  },
  fgoDetail: function (e) {
    let id = e.currentTarget.dataset.id;
    tt.navigateTo({
      url: `/pages/common/detail/fdetail/fdetail?id=${id}`
    });
  },
  switch(e) {
    // 第二个数据
    let tab = [];
    tt.showLoading({
      title: '加载中'
    });
    //
    let _this = this;
    tt.request({
      url: `${app.globalData.baseUrl}/release-work/work/${1}?typeOfWork=${this.data.typeOfWork}`,
      success: (res) => {
        let contents = res.data.dataMap.data;
        contents.records = contents.records.sort((item1, item2) => item2.gmtModified.localeCompare(item1.gmtModified));
        _this.setData({ contents });
      },
      complete: (res) => { tt.hideLoading(); },
    });
    this.setData({
      tabItems: tab,
      addShow: true,
    });
  },
  onShareAppMessage: function (res) {
    if (res.from === 'button') {
      // 来自页面内转发按钮
      console.log(res.target)
    }
    return {
      title: `我的名片`,
      path: '/page/user?id=123'
    }
  },
  onReachBottom: function () {
    if (this.data.isShow) {
      if (this.data.contents.current < this.data.contents.pages) {
        tt.showLoading();
        let contents = this.data.contents;
        let _this = this;
        tt.request({
          url: `${app.globalData.baseUrl}/release-work/work/${this.data.contents.current + 1}?typeOfWork=${this.data.typeOfWork}`,
          complete: (res) => { tt.hideLoading() },
          fail: (res) => { },
          success: (result) => {
            contents.current += 1;
            result.data.dataMap.data.records.forEach(item => contents.records.push(item));
            _this.setData({ contents });
            console.log(result.data.dataMap.data);
          },
        })
      }
    }
  }
})