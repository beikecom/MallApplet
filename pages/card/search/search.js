// pages/search/index.js
let app = getApp();
let input = null;
Page({
  data: {
    show: false,
    param: {
      page: 1,
      keyword: "",
      is_collect: 0
    },
    searchTitle: "输入姓名或公司或内容",
    focus: !0,
    keyWord: "",
    result: {}
  },

  setValue(e) {
    let keyWord = e.detail;
    clearTimeout(input);
    input = setTimeout(() => {
      this.setData({
        keyWord
      });

      if (this.data.keyWord) {
        this.getResult();
      } else {
        this.setData({
          result: {}
        });
      }
    }, 500);
  },

  // 清楚历史记录
  clearHistory(e) {
    tt.showLoading({
      title: '清楚中'
    });
    tt.removeStorageSync({
      key: 'history',
      complete: res => {
        tt.hideLoading();
      },
      fail: res => {},
      success: res => {
        this.setData({
          show: false
        });
      }
    });
  },

  onLoad: function (options) {},

  getResult(current = 1) {
    tt.showLoading({
      title: '检索中'
    });
    tt.request({
      url: `${app.globalData.baseUrl}/release-work/search/${current}?key=${this.data.keyWord}`,
      complete: res => {
        tt.hideLoading();
      },
      fail: res => {},
      success: res => {
        console.log(res.data.dataMap.data);
        this.setData({
          result: res.data.dataMap.data
        });
      }
    });
  }

});