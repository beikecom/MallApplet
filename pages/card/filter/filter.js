// pages/card/filter/filter.js
let area = require("../../../utils/area.js");

let app = getApp();
Page({
  data: {
    items: [["全部区域", false], ["全部行业", false], ["最新", false]],
    isShow: false,
    area: {},
    areaChild: [],
    timeList: [["最新"], ["热门"], ["最近"]],
    contents: []
  },

  getData(e) {
    const local = e.currentTarget.dataset.local;
    const time = e.currentTarget.dataset.time; // 传参数获取数据

    let list = this.data.items;
    list.forEach((item, index) => {
      if (index === 0 && local != null) item[0] = local;
      if (index === 2 && time != null) item[0] = time;
      item[1] = false;
    });
    this.setData({
      isShow: false,
      items: list
    });
  },

  getChild(e) {
    const index = e.currentTarget.dataset.index;
    let list = this.data.area[index][1];
    this.setData({
      areaChild: list
    });
  },

  getArea() {
    let areaList = {};
    area.area.forEach((item, index) => {
      let areaListChild = [];
      item.children.forEach((itemChild, index1) => {
        areaListChild.push(itemChild.title);
      });
      areaList[index] = [item.title, areaListChild];
    });
    this.setData({
      area: areaList
    });
  },

  checkFilter(e) {
    let current = e.currentTarget.dataset.current;
    let list = this.data.items;
    list.forEach((item, index) => {
      if (index === current) {
        item[1] = !item[1];
      } else {
        item[1] = false;
      }
    });
    this.setData({
      items: list,
      isShow: !this.data.isShow
    });
  },

  onLoad: function (options) {
    this.getArea();
    const title = options.title;
    const category = options.category;

    if (title != null) {
      tt.setNavigationBarTitle({
        title: title
      });
    }

    let current = 1;

    let _this = this;

    tt.request({
      url: `${app.globalData.baseUrl}/release-work/work/${current}?typeOfWork=${category}`,
      // 目标服务器url
      success: res => {
        let contents = _this.data.contents;
        contents = res.data.dataMap.data.records;

        _this.setData({
          contents
        });
      }
    });
  }
});