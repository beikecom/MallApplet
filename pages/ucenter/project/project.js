// pages/ucenter/project/project.js
let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    startTime: "",
    endTime: "",
    contentCount: 0,
    content: '',
    form: {
      pojectTitle: "",
      startTime: "",
      endTime: "",
      area: "",
      projectDetail: ""
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},
  bindStartTimeChange: function (e) {
    console.log("picker发送选择改变，携带值为", e.detail.value);
    let form = this.data.form;
    form.startTime = e.detail.value;
    this.setData({
      form,
      startTime: e.detail.value
    });
  },
  bindEndTimeChange: function (e) {
    console.log("picker发送选择改变，携带值为", e.detail.value);
    let form = this.data.form;
    form.endTime = e.detail.value;
    this.setData({
      form,
      endTime: e.detail.value
    });
  },

  pojectTitle(e) {
    let form = this.data.form;
    console.log(e);
    form.pojectTitle = e.detail.value;
    this.setData({
      form
    });
  },

  area(e) {
    let form = this.data.form;
    console.log(e);
    form.area = e.detail.value;
    this.setData({
      form
    });
  },

  projectDetail(e) {
    let form = this.data.form;
    console.log(e);
    form.projectDetail = e.detail.value;
    this.setData({
      form
    });
  },

  saveAnd(e) {
    this.commit();
    tt.navigateTo({
      url: '/pages/ucenter/project/project'
    });
  },

  save(e) {
    this.commit();
    tt.navigateTo({
      url: '/pages/ucenter/recruit/recruit'
    });
  },

  commit(e) {
    var that = this;
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/project-experience/addProject/${tt.getStorageSync('openid')}`,
      method: "POST",
      data: that.data.form,
      success: res => {
        tt.showToast({
          title: '提交成功',
          icon: 'none',
          duration: 4000
        });
      }
    });
  },

  handleContentInput(e) {
    let form = this.data.form;
    form.projectDetail = e.detail.value;
    this.setData({
      form,
      contentCount: e.detail.value.length //计算已输入的正文字数

    });
  }

});