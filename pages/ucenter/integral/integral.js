// pages/ucenter/integral/integral.js
let app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabItems: [["消耗记录", "", true], ["来源记录", "", false], ["获取积分", "", false]],
    integralInfo: {},
    current: 1,
    filterData: {
      orgin: "",
      category: "0"
    },
    userInfo: {},
  },

  setSwipterData(e) {
    const index = e.detail;
    let filterData = this.data.filterData;
    switch (index) {
      case 0:
        this.setData({ current: 1 });
        filterData.category = "0";
        this.getIntegral(this.data.current, filterData.category, filterData.origin);
        this.setData({ filterData })
        break;
      case 1:
        this.setData({ current: 1 });
        filterData.category = "";
        filterData.origin = "";
        this.getIntegral(this.data.current, filterData.category, filterData.origin);
        this.setData({ filterData })
        break;
      case 2:
        this.setData({ current: 1 });
        filterData.category = "1";
        this.getIntegral(this.data.current, filterData.category, filterData.origin);
        this.setData({ filterData })
        break;
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.getUsuerinfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let filterData = this.data.filterData;
    this.setData({ current: 1 })
    this.getIntegral(this.data.current, filterData.category, filterData.origin);
    tt.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let current = this.data.current;
    if (current < this.data.integralInfo.pages) {
      let filterData = this.data.filterData;
      this.setData({ current: current + 1 })
      this.getIntegral(current + 1, filterData.category, filterData.origin);
    }
  },

  getIntegral: function (current = 1, category = '0', origin = '') {
    let _this = this;
    let user = _this.data.userInfo;
    tt.showLoading({
      title: '加载中'
    });
    tt.request({
      url: `${app.globalData.baseUrl}/integral/selectAll/${current}?category=${category}&origin=${origin}&user=${user.id}`,
      complete: (res) => {
        tt.hideLoading();
      },
      fail: (res) => {
        tt.showToast({
          title: '加载失败',
          icon: "none"
        })
      },
      method: "GET",
      success: (result) => {
        let integral = result.data.dataMap.data;
        if (_this.data.current > 1) {
          // if (_this.data.current < _this.data.integralInfo.current)
          let integralInfo = _this.data.integralInfo;
          integral.records.forEach(item => { integralInfo.records.push(item) });
          _this.setData({ integralInfo });
        } else {
          _this.setData({ integralInfo: integral });
        }
      },
    })
  },
  getUsuerinfo: function () {
    let openid = tt.getStorageSync('openid');
    let _this = this;
    if (!openid) {
      tt.navigateBack({
        complete: (res) => {
          tt.showToast({
            title: '请登录',
            icon: "none",
          })
        }
      });
    } else {
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openid}`,
        complete: (res) => { },
        success: (result) => {
          _this.setData({ userInfo: result.data.dataMap.data });
          let filterData = _this.data.filterData;
          filterData.category = "0";
          _this.getIntegral(this.data.current, filterData.category, filterData.origin);
        },
      })
    }
  }
})