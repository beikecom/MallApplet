// pages/ucenter/message/message.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    messages: [],
    tabItems: [["未查看", "", true], ["已查看", "", false]],
    current: 0,
    userId: "",
  },

  goDetail: function (e) {
    let id = e.currentTarget.dataset.id;
    let work = e.currentTarget.dataset.work;
    let index = e.currentTarget.dataset.index;
    let _this = this;
    let list = ['删除', '标记为已查看', '查看详情']
    if (this.data.messages[index].message == "签到成功") {
      list = ['删除', '标记为已查看']
    }
    tt.showActionSheet({
      itemList: list,
      success: (res) => {
        let tapIndex = res.tapIndex;
        switch (tapIndex) {
          case 0:
            tt.showModal({
              title: '删除提醒',
              content: '确定删除吗',
              success: (result) => {
                if (result.confirm) {
                  tt.request({
                    url: `${app.globalData.baseUrl}/message/delete/${id}`,
                    method: "DELETE",
                    success: (res) => {
                      tt.showToast({
                        title: '删除成功',
                        success(res1) {
                          tt.startPullDownRefresh();
                        }
                      })
                    }
                  });
                }
              },
            });
            break;
          case 1:
            let form = this.data.messages[index];
            if (form.watched === 0) {
              tt.showLoading({
                title: '修改中',
              });
              form.watched = 1;
              tt.request({
                url: `${app.globalData.baseUrl}/message/watched`,
                complete: (res) => { tt.hideLoading() },
                data: form,
                fail: (res) => { },
                method: "PUT",
                success: (result) => {
                  tt.startPullDownRefresh();
                },
              })
            } else {
              tt.showToast({
                title: '不可重复修改',
                icon: "none"
              })
            }
            break;
          case 2:
            tt.navigateTo({
              url: `/pages/common/detail/detail?id=${work}` // 指定页面的url
            });
            break;
        }
      }
    });
  },

  setSwipterData(e) {
    const current = e.detail;
    this.setData({ current })
    let _this = this;
    switch (current) {
      case 0:
        _this.getUnWatched();
        break;
      case 1:
        _this.getUnWatched(1);
        break;
    }
  },

  onLoad: function (options) {
    let _this = this;
    let openId = tt.getStorageSync('openid');
    if (openId != undefined && openId != "" && openId != null) {
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openId}`,
        success(res) {
          let userInfo = res.data.dataMap.data;
          _this.setData({ userId: userInfo.id })
          _this.getUnWatched();
        }
      })
    } else {
      tt.navigateBack({
        complete: (res) => {
          tt.showToast({
            title: '请登录',
            icon: "none",
          })
        }
      });
    }
  },

  onPullDownRefresh: function () {
    this.getUnWatched(this.data.current);
    tt.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  changeStatus() {

  },
  getUnWatched(index = 0) {
    let _this = this;
    tt.showLoading({
      title: '加载中',
    })
    tt.request({
      url: `${app.globalData.baseUrl}/message/list/${_this.data.userId}?watched=${index}`,
      complete: (res) => { tt.hideLoading(); },
      fail: (res) => { },
      success: (result) => {
        _this.setData({ messages: result.data.dataMap.data });
      },
    })
  }
})