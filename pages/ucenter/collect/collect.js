// pages/ucenter/collect/collect.js
const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabItems: [["招工信息", "", true], ["招工记录", "", false]],
    current: 1,
    index: 0,
    collects: {},
    userInfo: {},
  },

  setSwipterData(e) {
    const index = e.detail;
    this.setData({ index })
    let current = this.data.current;
    switch (index) {
      case 0:
        this.onGetData(current, index);
        break;
      case 1:
        this.onGetData(current, index);
        break;
    }
  },

  goDetail(e) {
    let id = e.currentTarget.dataset.id;
    tt.navigateTo({
      url: `/pages/common/detail/detail?id=${id}`,
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.onGetUserInfo();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    let current = this.data.current;
    let index = this.data.index;
    this.onGetData(current, index);
    tt.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    let current = this.data.current;
    if (current < this.data.collects.pages) {
      let current = this.data.current;
      let index = this.data.index;
      this.onGetData(current, index);
    }
  },

  onGetData(current = 1, typeOfWork = 0) {
    let _this = this;
    let userId = this.data.userInfo.id;
    tt.showLoading({
      title: '加载中',
      success: (res) => {
        tt.request({
          url: `${app.globalData.baseUrl}/collect/page/${current}/${userId}?typeOfWork=${typeOfWork}`,
          complete: (res) => {
            tt.hideLoading();
          },
          fail: (res) => {
            tt.showToast({
              title: '加载失败',
              icon: "none",
            });
          },
          success: (result) => {
            if (result.data.dataMap.data == null) {
              _this.setData({ collects: {} });
            } else {
              _this.setData({ collects: result.data.dataMap.data });
            }
          },
        });
      },
    });
  },
  onGetUserInfo() {
    let openid = tt.getStorageSync('openid');
    let _this = this;
    if (!openid) {
      tt.navigateBack({
        complete: (res) => {
          tt.showToast({
            title: '请登录',
            icon: "none",
          })
        }
      });
    } else {
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openid}`,
        complete: (res) => { },
        success: (result) => {
          _this.setData({ userInfo: result.data.dataMap.data });
          _this.onGetData();
        },
      })
    }
  }
})