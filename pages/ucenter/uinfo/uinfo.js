// pages/ucenter/uinfo/uinfo.js
let app = getApp();
Page({
  data: {
    birthday: "",
    nation: [],
    workType: [],
    contentCount: 0,
    content: '',
    gender: ["", "男", "女"],
    form: {
      uname: "",
      area: "",
      phone: "",
      birthday: "",
      gender: "",
      nation: "",
      workType: "",
      self: ""
    },
    WorkTypeIndex: 0,
    nationIndex: 0,
    objectIndex: 0
  },
  onLoad: function (options) {},
  onShow: function () {
    var that = this;
    tt.request({
      url: `${app.globalData.baseUrl}/work-type/all`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('查询成功', res.data.dataMap.data);
        that.setData({
          workType: res.data.dataMap.data
        });
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/nation/getAllNation`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('查询成功', res.data.dataMap.data);
        that.setData({
          nation: res.data.dataMap.data
        });
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/is-show/select/isShow`,
      // 目标服务器url
      method: 'GET',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('是否伪装', res.data.dataMap.data);

        if (res.data.dataMap.data.isShow === 1) {
          this.setData({
            isShow: true
          });
        } else {
          this.setData({
            isShow: false
          });
        }
      }
    });
  },
  cancelLogin: function () {
    tt.navigateBack({
      delta: 1,

      success(res) {
        console.log(`${res}`);
      },

      fail(res) {
        console.log(`navigateBack调用失败`);
      }

    });
  },
  //生日
  bindDateChange: function (e) {
    console.log("picker发送选择改变，携带值为", e.detail.value);
    let form = this.data.form;
    form.birthday = e.detail.value;
    this.setData({
      form,
      birthday: e.detail.value
    });
  },

  bindNationChange(e) {
    console.log("picker发送选择改变，携带值为", e.detail.value);
    let form = this.data.form;
    form.nation = e.detail.value;
    this.setData({
      form,
      nationIndex: e.detail.value
    });
  },

  bindWorkTypeChange(e) {
    console.log("picker发送选择改变，携带值为", e.detail.value);
    let form = this.data.form;
    form.workType = e.detail.value;
    this.setData({
      form,
      WorkTypeIndex: e.detail.value
    });
  },

  bindGenderChange(e) {
    console.log("picker发送选择改变，携带值为", e.detail.value);
    let form = this.data.form;
    form.gender = e.detail.value;
    this.setData({
      form,
      objectIndex: e.detail.value
    });
  },

  commit(e) {
    var that = this;
    tt.request({
      //url: `https://localhost:10000/user/uInfo/${openid}`,
      url: `${app.globalData.baseUrl}/user/uInfo/${tt.getStorageSync('openid')}`,
      method: "POST",
      data: that.data.form,
      success: res => {
        tt.navigateTo({
          url: "/pages/ucenter/recruit/recruit",

          success(res1) {
            tt.showToast({
              title: '提交成功',
              icon: 'none',
              duration: 4000
            });
          }

        });
      }
    });
  },

  fail(res) {
    tt.showToast({
      title: '服务器真的错误了！',
      // 内容
      icon: "none"
    });
  },

  name(e) {
    let form = this.data.form;
    form.uname = e.detail.value;
    this.setData({
      form
    });
  },

  area(e) {
    let form = this.data.form;
    console.log(e);
    form.area = e.detail.name;
    this.setData({
      form
    });
  },

  phone(e) {
    let form = this.data.form;
    form.phone = e.detail.value;
    this.setData({
      form
    });
  },

  self(e) {
    let form = this.data.form;
    form.self = e.detail.value;
    this.setData({
      form
    });
  },

  handleContentInput(e) {
    let form = this.data.form;
    form.self = e.detail.value;
    this.setData({
      form,
      contentCount: e.detail.value.length //计算已输入的正文字数

    });
  }

});