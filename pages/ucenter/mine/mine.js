// pages/ucenter/mine/recruit.js
const app  =  getApp();
Page({
  data: {
    tabItems: [["全部", "", true], ["正在招/正在找", "", false], ["已招满/已找到", "", false]],
    userId: "",
    contents: [],
    actionSheetChange: true,
  },
  setSwipterData(e) {
    const current = e.detail;
    let _this = this;
    switch (current) {
      case 0:
        tt.showLoading({
          title: '加载中',
        });
        tt.request({
          url: `${app.globalData.baseUrl}/release-work/work/mine/${_this.data.userId}?status=0`, // 目标服务器url
          success: (res) => {
            _this.setData({ contents: res.data.dataMap.data });
          },
          complete() {
            tt.hideLoading();
          }
        });
        break;
      case 1:
        tt.showLoading({
          title: '加载中',
        });
        tt.request({
          url: `${app.globalData.baseUrl}/release-work/work/mine/${_this.data.userId}?status=1`, // 目标服务器url
          success: (res) => {
            _this.setData({ contents: res.data.dataMap.data });
          },
          fail(res) {
            _this.setData({ contents: [] });
          },
          complete() {
            tt.hideLoading();
          }
        });
        break;
      case 2:
        tt.showLoading({
          title: '加载中', // 内容
        });
        tt.request({
          url: `${app.globalData.baseUrl}/release-work/work/mine/${_this.data.userId}?status=2`, // 目标服务器url
          success: (res) => {
            _this.setData({ contents: res.data.dataMap.data });
          },
          fail(res) {
            _this.setData({ contents: [] });
          },
          complete() {
            tt.hideLoading();
          }
        });
        break;
    }
  },
  goDetail: function (e) {
    let id = e.currentTarget.dataset.id;
    let index = e.currentTarget.dataset.index;
    let _this = this;
    const itemList = ["查看详情", "已招满", "删除"];
    tt.showActionSheet({
      itemList,
      success(res) {
        switch (res.tapIndex) {
          case 0:
            tt.navigateTo({
              url: `/pages/common/detail/detail?id=${id}` // 指定页面的url
            });
            break;
          case 1:
            let content = _this.data.contents[index];
            if (content.status === 1) {
              content.status = 2;
            } else {
              tt.showToast({
                title: '状态不可修改', // 内容
                icon: "none"
              });
              return false;
            }
            tt.request({
              url: `${app.globalData.baseUrl}/release-work/work`, // 目标服务器url
              method: "put",
              data: content,
              success: (res) => {
                tt.navigateTo({
                  url: '/pages/ucenter/mine/mine' // 指定页面的url
                });
                tt.showToast({ title: '修改成功' });
              }
            });
            break
          case 2:
            tt.showModal({
              title: "提示",
              content: "确定删除吗",
              success(res) {
                if (res.confirm) {
                  tt.request({
                    url: `${app.globalData.baseUrl}/release-work/work/${id}`, // 目标服务器url
                    method: "delete",
                    success: (res) => {
                      tt.navigateTo({
                        url: '/pages/ucenter/mine/mine' // 指定页面的url
                      });
                      tt.showToast({ title: '删除成功' });
                    }
                  });
                } else if (res.cancel) {
                } else {
                }
              },
              fail(res) {
                console.log(`showModal调用失败`);
              },
            });
            break
        }
      },
      fail(res) { },
    });
  },
  onLoad: function (options) {
    let _this = this;
    // 获取用户Id
    let openId = tt.getStorageSync('openid');
    if (openId != null && openId != '' && openId != undefined) {
      tt.showLoading({
        title: '加载中', // 内容
      });
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openId}`, // 目标服务器url
        success: (res) => {
          let userId = res.data.dataMap.data.id;
          _this.setData({ userId });
          tt.request({
            url: `${app.globalData.baseUrl}/user/userReleaseWorkListById/${_this.data.userId}`, // 目标服务器url
            success: (res) => {
              _this.setData({ contents: res.data.dataMap.data });
            }
          });
        },
        complete() {
          tt.hideLoading();
        }
      });
    } else {
      tt.navigateBack({
        complete: (res) => {
          tt.showToast({
            title: '请登录',
            icon: "none",
          })
        }
      });
    }
  },
  onShow: function () {

  },
  onReachBottom: function () {
    
  }
})