// pages/ucenter/skill/skill.js
let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    date: '2020-08-17',
    nowDate: '2020-08-17',
    form: {
      userId: "",
      openId: "",
      skillName: "",
      time: "2020-08-17",
      imageUrl: ""
    }
  },

  skillInput(e) {
    let form = this.data.form;
    form.skillName = e.detail.value;
    this.setData({
      form
    });
  },

  saveAnd(e) {
    this.saveSkill();
    tt.navigateTo({
      url: '/pages/ucenter/skill/skill'
    });
  },

  save(e) {
    this.saveSkill();
    tt.navigateTo({
      url: '/pages/ucenter/recruit/recruit'
    });
  },

  bindDateChange(e) {
    let form = this.data.form;
    form.time = e.detail.value;
    this.setData({
      nowDate: e.detail.value,
      form
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.onGetUserInfo();
    this.getNowDate();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {},

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {},

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {},

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {},

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {},

  getNowDate() {
    var date = new Date();
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    var day = date.getDate();

    if (month < 10) {
      month = "0" + month;
    }

    if (day < 10) {
      day = "0" + day;
    }

    var nowDate = year + '-' + month + '-' + day;
    this.setData({
      date: nowDate
    });
  },

  saveSkill() {
    tt.showLoading({
      title: '保存中'
    });
    let form = this.data.form;
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/skills-certificate`,
      complete: res => {
        tt.hideLoading();
      },
      data: form,
      fail: res => {
        tt.showToast({
          title: '保存失败',
          icon: "none"
        });
      },
      method: "POST",
      success: result => {
        console.log(result);
      }
    });
  },

  onGetUserInfo() {
    let openid = tt.getStorageSync('openid');

    let _this = this;

    if (!openid) {
      tt.navigateBack({
        complete: res => {
          tt.showToast({
            title: '请登录',
            icon: "none"
          });
        }
      });
    } else {
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openid}`,
        complete: res => {},
        success: result => {
          let form = _this.data.form;
          form.openId = openid;
          form.userId = result.data.dataMap.data.id;

          _this.setData({
            form
          });
        }
      });
    }
  }

});