let app =  getApp();
  
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow: false,
    ttlogin: false,
    message: 0
  },
  onLoad(){
    var that = this;
    let userInfoParam = tt.getStorageSync('userInfo')
    if(userInfoParam){
      this.setData({
        ttlogin: true,
        apiUserInfoMap: userInfoParam
      });
    }else{
      this.setData({
        ttlogin: false
      });
    }

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/is-show/select/isShow`, // 目标服务器url
      method: 'GET',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('是否伪装',res.data.dataMap.data);
        if(res.data.dataMap.data.isShow === 1){
          this.setData({
            isShow: true
          });
        }else{
          this.setData({
            isShow: false
          });
        }
      }
    });
  },
  onshow: function () {
    var that = this;
    var userInfoParam = tt.getStorageSync('userInfo')
    console.log(userInfoParam);
    if(userInfoParam){
      this.setData({
        ttlogin: true,
        apiUserInfoMap: userInfoParam
      });
    }else{
      that.setData({
        ttlogin: false
      });
    }

    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/is-show/select/isShow`, // 目标服务器url
      method: 'GET',
      header: {
        'content-type': 'application/json'
      },
      success: (res) => {
        console.log('是否伪装',res.data.dataMap.data);
        if(res.data.dataMap.data.isShow === 1){
          this.setData({
            isShow: false
          });
        }else{
          this.setData({
            isShow: true
          });
        }
      }
    });
  },
  showLogin: function () {
    this.setData({
      ttlogin: false,
    });
  },
  cancelLogin() {
    this.setData({
      ttlogin: true,
    });
  },
  processLogin: function(e){
    app.globalHandleLogin(e);
    this.setData({
      apiUserInfoMap: tt.getStorageSync("userInfo")
    });
    console.log('qwe',app.globalData.userInfoData);
  },
  onShow() {this.getMessage();},
  getMessage() {
    let openid = tt.getStorageSync('openid');
    let _this = this;
    if (!openid) {
      tt.showToast({
        title: '未登录',
        icon: "none"
      })
    } else {
      tt.request({
        url: `${app.globalData.baseUrl}/user/selectIsUser/${openid}`,
        complete: (res) => { },
        success: (result) => {
          tt.request({
            url: `${app.globalData.baseUrl}/message/count/${result.data.dataMap.data.id}`,
            success(res1) {
              _this.setData({message: res1.data.dataMap.data})
            }
          })
        },
      })
    }
  }

  // //授权登录绑定按钮
  // processLogin: function (e) {
  //   var that = this;
  //   console.log('111', e.detail.userInfo);
  //   //登录
  //   that.login();
  //   if (!tt.getStorageSync('userInfo')) {
  //     //获取用户信息
  //     console.log('用户信息', e.detail.userInfo);
  //     that.setData({
  //       apiUserInfoMap: e.detail.userInfo
  //     });
  //     tt.setStorageSync('userInfo', e.detail.userInfo)
  //     tt.setStorageSync('openid', e.detail.userInfo.openid);
  //     tt.setStorageSync('nick_name', e.detail.userInfo.nickName);
  //     tt.setStorageSync('avatar_url', e.detail.userInfo.avatarUrl);
  //     tt.setStorageSync('gender', e.detail.userInfo.gender);
  //   } else if (tt.getStorageSync('userInfo')) {
  //     that.setData({
  //       apiUserInfoMap: tt.getStorageSync('userInfo'),
  //       ttlogin: true
  //     });
  //   }
  // },

  // //登录
  // login: function () {
  //   var that = this;
  //   tt.login({
  //     success(res) {
  //       console.log('login调用成功', res.code);
  //       var code = res.code;
  //       //获取用户openid
  //       tt.request({
  //         url: `${app.globalData.baseUrl}/oauth/tt/getOpenid/${code}`,
  //         method: 'GET',
  //         dataType: 'json',
  //         header: {
  //           'content-type': 'application/json'
  //         },
  //         success: (res) => {
  //           console.log('成功获取openid', JSON.parse(res.data.message).openid)
  //           tt.setStorageSync('openid', JSON.parse(res.data.message).openid);
  //           console.log('查询数据库中是否有该用户数据', tt.getStorageSync('openid'));
  //           tt.request({
  //             url: `${app.globalData.baseUrl}/user/selectIsUser/${tt.getStorageSync('openid')}`, // 目标服务器url
  //             method: 'GET',
  //             dataType: 'json',
  //             header: {
  //               'content-type': 'application/json'
  //             },
  //             success: (res) => {
  //               console.log('数据库中是否存在用户数据', res.data);
  //               if (res.data.message === '用户不存在') {
  //                 //用户注册
  //                 that.register();
  //               } else {
  //                 that.setData({
  //                   apiUserInfoMap: tt.getStorageSync('userInfo'),
  //                   ttlogin: true
  //                 });
  //               }
  //             }
  //           });
  //         },
  //         fail(res) {
  //           console.log('获取openid失败');
  //         }
  //       })
  //     },
  //     fail(res) {
  //       console.log(`login调用失败`);
  //     },
  //   });
  // },

  // //查询数据库中是否有该用户数据
  // // getIsUser(){
  // //   var that = this;
  // //   console.log('查询数据库中是否有该用户数据',tt.getStorageSync('openid'));
  // //   tt.request({
  // //     url: `${app.globalData.baseUrl}/user/selectIsUser/${tt.getStorageSync('openid')}`, // 目标服务器url
  // //     method: 'GET',
  // //     dataType:'json',
  // //     header: {
  // //       'content-type': 'application/json'
  // //     },
  // //     success: (res) => {
  // //       console.log('数据库中是否存在用户数据',res.data);
  // //       if(res.data.message === '用户不存在'){
  // //         //用户注册
  // //         that.register();
  // //       }else{
  // //         that.setData({
  // //           apiUserInfoMap: tt.getStorageSync('userInfo'),
  // //           ttlogin: true
  // //         });          
  // //       }
  // //     }
  // //   });
  // // },

  // //用户注册
  // register: function () {
  //   tt.request({
  //     url: `${app.globalData.baseUrl}/user/register`, // 目标服务器url
  //     method: 'POST',
  //     dataType: 'json',
  //     header: {
  //       'content-type': 'application/x-www-form-urlencoded'
  //     },
  //     data: {
  //       openid: tt.getStorageSync('openid'),
  //       nickName: tt.getStorageSync('nick_name'),
  //       avatarUrl: tt.getStorageSync('avatar_url'),
  //       gender: tt.getStorageSync('gender'),
  //     },
  //     success: (res) => {
  //       var constant = res.data.message
  //       console.log('注册成功', constant)
  //     }
  //   });
  // }
})