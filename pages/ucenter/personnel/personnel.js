// pages/ucenter/personnel/personnel.js
let app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    proficiency: ["", "学徒工（小工）", "中级工（中工）", "高级工（大工）"],
    composition: ["", "个人", "班组", "施工队", "突击队"],
    form: {
      proficiency: "",
      composition: "",
      workAge: "",
      home: "",
      num: 1
    },
    proficiencyIndex: 0,
    compositionIndex: 0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {},

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {},

  bindProficiencyChange(e) {
    console.log("picker发送选择改变，携带值为", e.detail.value);
    let form = this.data.form;
    form.proficiency = e.detail.value;
    this.setData({
      form,
      proficiencyIndex: e.detail.value
    });
  },

  bindStaffCompositionChange(e) {
    console.log("picker发送选择改变，携带值为", e.detail.value);
    let form = this.data.form;
    form.composition = e.detail.value;
    this.setData({
      form,
      compositionIndex: e.detail.value
    });
  },

  workAge(e) {
    let form = this.data.form;
    form.workAge = e.detail.value;
    this.setData({
      form
    });
  },

  home(e) {
    let form = this.data.form;
    form.home = e.detail.value;
    this.setData({
      form
    });
  },

  num(e) {
    let form = this.data.form;
    form.num = e.detail.value;
    this.setData({
      form
    });
  },

  commit(e) {
    var that = this;
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/personnel-info/getInfo/${tt.getStorageSync('openid')}`,
      method: "GET",
      success: res => {
        console.log(res.data.dataMap.data);

        if (res.data.dataMap.data) {
          tt.request({
            url: `${app.globalData.baseUrl}/mallappletbackstage/personnel-info/upDataInfo/${tt.getStorageSync('openid')}`,
            method: "POST",
            data: that.data.form,
            success: res => {
              tt.navigateTo({
                url: "/pages/ucenter/recruit/recruit",

                success(res1) {
                  tt.showToast({
                    title: '更新成功',
                    icon: 'none',
                    duration: 4000
                  });
                }

              });
            }
          });
        } else {
          tt.request({
            url: `${app.globalData.baseUrl}/mallappletbackstage/personnel-info/addInfo/${tt.getStorageSync('openid')}`,
            method: "POST",
            data: that.data.form,
            success: res => {
              tt.navigateTo({
                url: "/pages/ucenter/recruit/recruit",
                success(res1) {
                  tt.showToast({
                    title: '提交成功',
                    icon: 'none',
                    duration: 4000
                  });
                }

              });
            }
          });
        }
      },

      fail(res) {
        console.log(res);
      }

    });
  }

});