// pages/ucenter/recruit/recruit.js
let app = getApp();
Page({
  data: {
    isShow: false,
    isNull: true,
    personnelIsNull: true,
    projectIsNull: true,
    skillsIsNull: true,
    ttlogin: false,
    info: {
      uname: "",
      area: "",
      phone: "",
      gender: "",
      nation: "",
      workType: "",
      self: "",
      sex: ""
    },
    personnel: {},
    project: [],
    skills: []
  },
  onLoad: function (options) {
    var that = this;
    var userInfoParam = tt.getStorageSync('userInfo');
    console.log(userInfoParam);

    if (userInfoParam) {
      that.setData({
        ttlogin: true,
        apiUserInfoMap: userInfoParam
      });
    } else {
      that.setData({
        ttlogin: false
      });
    }

    tt.request({
      url: `${app.globalData.baseUrl}/user/info/${tt.getStorageSync("openid")}`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('查询成功', res.data);

        if (res.data.message === "成功") {
          that.setData({
            info: res.data.dataMap.data,
            isNull: true
          });
        } else {
          that.setData({
            isNull: false
          });
        }
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/personnel-info/getInfo/${tt.getStorageSync("openid")}`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('人员信息', res.data);

        if (res.data.message === "查询成功") {
          that.setData({
            personnel: res.data.dataMap.data,
            personnelIsNull: false
          });
        } else {
          that.setData({
            personnelIsNull: true
          });
        }
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/project-experience/getProject/${tt.getStorageSync("openid")}`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('项目经验', res.data);
        
        if(!res.data.dataMap.data){
          that.setData({
            personnel: res.data.dataMap.data,
            personnelIsNull: false
          });
        }else{
          that.setData({
            personnelIsNull: true
          });
        }
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/skills-certificate/getSkills/${tt.getStorageSync("openid")}`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('职业技能', res.data);

        if (res.data.dataMap.data.length > 0) {
          that.setData({
            skills: res.data.dataMap.data,
            skillsIsNull: false
          });
        } else {
          that.setData({
            skillsIsNull: true
          });
        }
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/is-show/select/isShow`,
      // 目标服务器url
      method: 'GET',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('是否伪装', res.data.dataMap.data);

        if (res.data.dataMap.data.isShow === 1) {
          this.setData({
            isShow: true
          });
        } else {
          this.setData({
            isShow: false
          });
        }
      }
    });
  },
  onShwo: function () {
    tt.request({
      url: `${app.globalData.baseUrl}/user/info/${tt.getStorageSync("openid")}`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('查询成功', res.data);
        that.setData({
          info: res.data.dataMap.data
        });
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/personnel-info/getInfo/${tt.getStorageSync("openid")}`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('查询成功', res.data);
        if(!res.data.dataMap.data){
          that.setData({
            personnel: res.data.dataMap.data,
            personnelIsNull: false
          });
        }else{
          that.setData({
            personnelIsNull: true
          });
        }
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/project-experience/getProject/${tt.getStorageSync("openid")}`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('项目经验', res.data);
        that.setData({
          project: res.data.dataMap.data
        });
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/skills-certificate/getSkills/${tt.getStorageSync("openid")}`,
      // 目标服务器url
      method: 'GET',
      dataType: 'json',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('职业技能', res.data);
        that.setData({
          skills: res.data.dataMap.data
        });
      }
    });
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/is-show/select/isShow`,
      // 目标服务器url
      method: 'GET',
      header: {
        'content-type': 'application/json'
      },
      success: res => {
        console.log('是否伪装', res.data.dataMap.data);

        if (res.data.dataMap.data.isShow === 1) {
          this.setData({
            isShow: true
          });
        } else {
          this.setData({
            isShow: false
          });
        }
      }
    });
  },
  basic_info: function () {
    tt.navigateTo({
      url: "/pages/ucenter/uinfo/uinfo"
    });
  },
  personnel_info: function () {
    tt.navigateTo({
      url: "/pages/ucenter/personnel/personnel"
    });
  },
  project: function () {
    tt.navigateTo({
      url: "/pages/ucenter/project/project"
    });
  },
  occupation: function () {
    tt.navigateTo({
      url: "/pages/ucenter/skill/skill"
    });
  },
  updata_text: function () {
    tt.navigateTo({
      url: "/pages/ucenter/uinfo/uinfo"
    });
  },
  updata_personnel: function () {
    tt.navigateTo({
      url: "/pages/ucenter/personnel/personnel"
    });
  },
  updata_project: function () {
    tt.navigateTo({
      url: "/pages/ucenter/project/project"
    });
  },
  updata_skills: function () {
    tt.navigateTo({
      url: "/pages/ucenter/skill/skill"
    });
  },
  cancelLogin: function () {
    tt.navigateBack({
      delta: 1,

      success(res) {
        console.log(`${res}`);
      },

      fail(res) {
        console.log(`navigateBack调用失败`);
      }

    });
  },
  processLogin: function (e) {
    var that = this;
    app.globalHandleLogin(e);
    if(!tt.getStorageSync("userInfo")){
      that.setData({
        ttlogin: false
      });
    }else if(tt.getStorageSync("userInfo")){
      that.setData({
         apiUserInfoMap: tt.getStorageSync("userInfo"),
        ttlogin: true
      });
    }
  },
  deleteProject: function (e) {
    console.log('获取的id值', e.currentTarget.dataset.id);
    tt.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/project-experience/deleteProject/${e.currentTarget.dataset.id}`,
      // 目标服务器url
      method: 'DELETE',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: res => {
        var constant = res.data.message;
        console.log('删除成功', constant);
        tt.navigateTo({
          url: '/pages/ucenter/recruit/recruit'
        });
      }
    });
  },
  deleteSkill: function(e){
    console.log('获取的id值',e.currentTarget.dataset.id)
    wx.request({
      url: `${app.globalData.baseUrl}/mallappletbackstage/skills-certificate/deleteSkill/${e.currentTarget.dataset.id}`,  // 目标服务器url
      method: 'DELETE',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: (res) => {
        var constant = res.data.message
        console.log('删除成功',constant)
        wx.navigateTo({
          url: '/pages/ucenter/recruit/recruit'
        })
      }
    });
  },
  shareTo: function(){
    wx.navigateTo({
      url: '/pages/card/index/index'
    })
  }
});