let app =  getApp();
  
Page({

  /**
   * 页面的初始数据
   */
  data: {
    form: {
      username: "",
      password: ""
    },
  },
  username(e) {
    let form = this.data.form;
    form.username = e.detail.value;
    this.setData({ form })
  },
  password(e) {
    let form = this.data.form;
    form.password = e.detail.value;
    this.setData({ form })
  },
  commit(e){
    var that = this;
    wx.request({
      url: `${app.globalData.baseUrl}/user/perfectInfo/${wx.getStorageSync('openid')}`,
      method: "POST",
      data: that.data.form,
      success: (res) => {
        wx.navigateBack({
          delta: 1,
          success(res1) {
            wx.showToast({
              title: '提交成功',
              icon: 'none',
              duration: 4000
            });
          }
        });
      }
    });
  },
})